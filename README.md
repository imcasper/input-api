# input-api

Fixed API for input devices. It is usually used for the internal needs of other libraries.

## deploy

### Local
Just add water:
- `./gradlew publishToMavenLocal`

### Nexus
Need variables (ask the administrator):
`ossrhUsername`,
`ossrhPassowrd`,
`signing.keyId`,
`signing.password`,
`signing.secretKeyRingFile`.


Publishing, closing, releasing:
- `./gradlew publishToSonatype`
- `./gradlew findSonatypeStagingRepository closeAndReleaseSonatypeStagingRepository`

In case of troubles, see also:
- https://s01.oss.sonatype.org/#stagingRepositories
- https://github.com/gradle-nexus/publish-plugin