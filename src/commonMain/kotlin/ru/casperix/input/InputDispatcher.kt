package ru.casperix.input

import ru.casperix.signals.concrete.Promise

interface InputDispatcher : InputReceiver {
    override val onKeyTyped: Promise<KeyTyped>
    override val onKeyDown: Promise<KeyDown>
    override val onKeyUp: Promise<KeyUp>

    override val onMouseWheel: Promise<MouseWheel>
    override val onMouseMove: Promise<MouseMove>

    override val onTouchDragged: Promise<TouchMove>
    override val onTouchUp: Promise<TouchUp>
    override val onTouchDown: Promise<TouchDown>
}