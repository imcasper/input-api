package ru.casperix.input

class InputQueue(source: InputDispatcher) {
    private val inputs = mutableListOf<Entry>()

    class Entry(val dispatcher: InputDispatcher, val priority: Int)

    init {
        source.onKeyDown.then { event -> inputs.forEach { entry -> entry.dispatcher.onKeyDown.set(event) } }
        source.onKeyTyped.then { event -> inputs.forEach { entry -> entry.dispatcher.onKeyTyped.set(event) } }
        source.onKeyUp.then { event -> inputs.forEach { entry -> entry.dispatcher.onKeyUp.set(event) } }
        source.onMouseMove.then { event -> inputs.forEach { entry -> entry.dispatcher.onMouseMove.set(event) } }
        source.onMouseWheel.then { event -> inputs.forEach { entry -> entry.dispatcher.onMouseWheel.set(event) } }
        source.onTouchDown.then { event -> inputs.forEach { entry -> entry.dispatcher.onTouchDown.set(event) } }
        source.onTouchDragged.then { event -> inputs.forEach { entry -> entry.dispatcher.onTouchDragged.set(event) } }
        source.onTouchUp.then { event -> inputs.forEach { entry -> entry.dispatcher.onTouchUp.set(event) } }
    }

    fun addDispatcher(inputDispatcher: InputDispatcher, priority: Int) {
        removeDispatcher(inputDispatcher)

        inputs += Entry(inputDispatcher, priority)
        inputs.sortBy { entry -> -entry.priority }
    }

    fun removeDispatcher(inputDispatcher: InputDispatcher) {
        inputs.removeAll { entry -> entry.dispatcher == inputDispatcher }
    }
}