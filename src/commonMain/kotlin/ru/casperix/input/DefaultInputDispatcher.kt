package ru.casperix.input

import ru.casperix.signals.concrete.Signal

class DefaultInputDispatcher : InputDispatcher {
    override val onKeyDown = Signal<KeyDown>()
    override val onKeyTyped = Signal<KeyTyped>()
    override val onKeyUp = Signal<KeyUp>()

    override val onMouseMove = Signal<MouseMove>()
    override val onMouseWheel = Signal<MouseWheel>()

    override val onTouchDown = Signal<TouchDown>()
    override val onTouchDragged = Signal<TouchMove>()
    override val onTouchUp = Signal<TouchUp>()

//    override val onButtonDown = Signal.pipe(onKeyDown.transform { ButtonEvent(it.button, it.captured) },
//        onTouchDown.transform { ButtonEvent(it.button, it.captured) })
//    override val onButtonUp =
//        Signal.pipe(onKeyUp.transform { ButtonEvent(it.button, it.captured) }, onTouchUp.transform { ButtonEvent(it.button, it.captured) })
}