package ru.casperix.input

enum class KeyButton(
    @Deprecated(message = "Its not scan-code. For other platform can any number")
    val code: Int) : Button {
    UNKNOWN_KEY(-1),
    BACK_QUOTE(41),

    NUM_0(7),
    NUM_1(8),
    NUM_2(9),
    NUM_3(10),
    NUM_4(11),
    NUM_5(12),
    NUM_6(13),
    NUM_7(14),
    NUM_8(15),
    NUM_9(16),

    BACK(4),
    AT(77),
    CALL(5),
    CAMERA(27),
    CLEAR(28),
    CENTER(23),
    ENDCALL(6),
    ENVELOPE(65),
    EXPLORER(64),
    FOCUS(80),
    GRAVE(68),
    HEADSETHOOK(79),
    MEDIA_FAST_FORWARD(90),
    MEDIA_NEXT(87),
    MEDIA_PLAY_PAUSE(85),
    MEDIA_PREVIOUS(88),
    MEDIA_REWIND(89),
    MEDIA_STOP(86),
    NOTIFICATION(245),
    NUM(78),
    POUND(18),
    SEARCH(84),
    SOFT_LEFT(1),
    SOFT_RIGHT(2),
    STAR(17),
    SYM(63),
    META_ALT_LEFT_ON(16),
    META_ALT_ON(2),
    META_ALT_RIGHT_ON(32),
    META_SHIFT_LEFT_ON(64),
    META_SHIFT_ON(1),
    META_SHIFT_RIGHT_ON(128),
    META_SYM_ON(4),
    PICTSYMBOLS(94),
    SWITCH_CHARSET(95),
    NUMPAD_LEFT_PAREN(162),
    NUMPAD_RIGHT_PAREN(163),
    BUTTON_CIRCLE(255),

    AUDIO_VOLUME_MUTE(91),
    AUDIO_VOLUME_DOWN(25),
    AUDIO_VOLUME_UP(24),

    ARROW_DOWN(20),
    ARROW_LEFT(21),
    ARROW_RIGHT(22),
    ARROW_UP(19),
    ALT_LEFT(57),
    ALT_RIGHT(58),
    APOSTROPHE(75),
    BACKSPACE(67),
    BACKSLASH(73),
    CAPS_LOCK(115),
    COMMA(55),
    DELETE(112),
    ENTER(66),
    EQUALS(70),
    HOME(3),
    LEFT_BRACKET(71),
    CONTEXT_MENU(82),
    MINUS(69),
    PAUSE(121), // aka break
    PERIOD(56),
    PLUS(81),
    PRINT_SCREEN(120), // aka SYSRQ
    RIGHT_BRACKET(72),
    SCROLL_LOCK(116),
    SEMICOLON(74),
    SHIFT_LEFT(59),
    SHIFT_RIGHT(60),
    SLASH(76),
    SPACE(62),
    TAB(61),
    CONTROL_LEFT(129),
    CONTROL_RIGHT(130),
    ESCAPE(111),
    END(123),
    INSERT(124),
    PAGE_UP(92),
    PAGE_DOWN(93),
    NUM_LOCK(143),
    COLON(243),
    SUPER_USER_LEFT(347),
    SUPER_USER_RIGHT(348),

    A(29),
    B(30),
    C(31),
    D(32),
    E(33),
    F(34),
    G(35),
    H(36),
    I(37),
    J(38),
    K(39),
    L(40),
    M(241),
    N(42),
    O(43),
    P(44),
    Q(45),
    R(46),
    S(47),
    T(48),
    U(49),
    V(50),
    W(51),
    X(52),
    Y(53),
    Z(54),

    BUTTON_A(96),
    BUTTON_B(97),
    BUTTON_C(98),
    BUTTON_X(99),
    BUTTON_Y(100),
    BUTTON_Z(101),
    BUTTON_L1(102),
    BUTTON_R1(103),
    BUTTON_L2(104),
    BUTTON_R2(105),
    BUTTON_THUMBL(106),
    BUTTON_THUMBR(107),
    BUTTON_START(108),
    BUTTON_SELECT(109),
    BUTTON_MODE(110),

    NUMPAD_0(144),
    NUMPAD_1(145),
    NUMPAD_2(146),
    NUMPAD_3(147),
    NUMPAD_4(148),
    NUMPAD_5(149),
    NUMPAD_6(150),
    NUMPAD_7(151),
    NUMPAD_8(152),
    NUMPAD_9(153),

    NUMPAD_DIVIDE(154),
    NUMPAD_MULTIPLY(155),
    NUMPAD_SUBTRACT(156),
    NUMPAD_ADD(157),
    NUMPAD_DOT(158),
    NUMPAD_DECIMAL(83),
    NUMPAD_ENTER(160),
    NUMPAD_EQUALS(161),


    F1(131),
    F2(132),
    F3(133),
    F4(134),
    F5(135),
    F6(136),
    F7(137),
    F8(138),
    F9(139),
    F10(140),
    F11(141),
    F12(142),
    F13(183),
    F14(184),
    F15(185),
    F16(186),
    F17(187),
    F18(188),
    F19(189),
    F20(190),
    F21(191),
    F22(192),
    F23(193),
    F24(194),

    SLEEP(240),
    POWER(26),
    WAKE(242),

    INTL_BACKSLASH(1001),
    ;

    companion object {
        private val map = values().associateBy { it.code }

        fun getByCode(code: Int): KeyButton {
            return map[code] ?: UNKNOWN_KEY
        }
    }
}