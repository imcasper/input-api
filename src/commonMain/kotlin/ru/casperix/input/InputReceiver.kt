package ru.casperix.input

import ru.casperix.signals.concrete.Future


interface InputReceiver {
    val onKeyTyped: Future<KeyTyped>
    val onKeyDown: Future<KeyDown>
    val onKeyUp: Future<KeyUp>

    val onMouseWheel: Future<MouseWheel>
    val onMouseMove: Future<MouseMove>

    val onTouchDragged: Future<TouchMove>
    val onTouchUp: Future<TouchUp>
    val onTouchDown: Future<TouchDown>

//    val onButtonDown: Future<ButtonEvent>
//    val onButtonUp: Future<ButtonEvent>
}

