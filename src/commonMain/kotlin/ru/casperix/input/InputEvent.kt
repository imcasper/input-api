package ru.casperix.input

import ru.casperix.math.vector.float32.Vector2f

interface InputEvent {
    var captured: Boolean
}

abstract class AbstractInputEvent() : InputEvent {
    override var captured: Boolean = false
}

//class ButtonEvent(val button: Button) : InputEvent() {
//    constructor(button: Button, captured: Boolean) : this(button) {
//        this.captured = captured
//    }
//}

interface KeyEvent : InputEvent {
    val button: KeyButton
}

class KeyTyped(val char: Char, override val button: KeyButton) : KeyEvent, AbstractInputEvent()

class KeyUp(override val button: KeyButton) : KeyEvent, AbstractInputEvent()
class KeyDown(override val button: KeyButton) : KeyEvent, AbstractInputEvent()

interface PointerEvent : InputEvent {
    val position: Vector2f
}

interface PointerUp : PointerEvent {
    val button: PointerButton
}

interface PointerMove : PointerEvent {

}

interface PointerDown : PointerEvent {
    val button: PointerButton
}


interface TouchEvent : PointerEvent {
    val pointer: Int
}

class TouchDown(override val position: Vector2f, override val pointer: Int, override val button: PointerButton) :
    TouchEvent, PointerDown, AbstractInputEvent()

class TouchMove(override val position: Vector2f, override val pointer: Int) :
    TouchEvent, PointerMove, AbstractInputEvent()

class TouchUp(override val position: Vector2f, override val pointer: Int, override val button: PointerButton) :
    TouchEvent, PointerUp, AbstractInputEvent()


interface MouseEvent : PointerEvent

class MouseDown(override val position: Vector2f, override val button: PointerButton) :
    MouseEvent, PointerDown, AbstractInputEvent()

class MouseMove(override val position: Vector2f) :
    MouseEvent, PointerMove, AbstractInputEvent()

class MouseUp(override val position: Vector2f,  override val button: PointerButton) :
    MouseEvent, PointerUp, AbstractInputEvent()



data class MouseWheel(override val position: Vector2f, val wheel: Vector2f) :
    MouseEvent, AbstractInputEvent()
