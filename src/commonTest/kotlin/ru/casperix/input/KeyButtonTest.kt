package ru.casperix.input

import kotlin.test.Test
import kotlin.test.assertTrue

class KeyButtonTest {
	@Test
	fun checkDuplicateKey() {
		val map = mutableMapOf<Int, KeyButton>()
		val duplicated = mutableListOf<Pair<KeyButton, KeyButton>>()
		KeyButton.values().forEach {
			if (map.containsKey(it.code)) {
				duplicated += Pair(map[it.code]!!, it)
			} else {
				map[it.code] = it
			}
		}
		assertTrue(duplicated.isEmpty(), "duplicated keys: $duplicated")
	}
}